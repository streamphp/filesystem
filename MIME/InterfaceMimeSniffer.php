<?php

/**
 * This file is part of the Stream\Filesystem Package
 *
 * (c) Thomas Appel <mail@thomas-appel.com
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem\MIME;

/**
 * InterfaceMimeSniffer
 *
 * @interface
 * @package Stream\Filesystem
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
interface InterfaceMimeSniffer
{
    /**
     * get the mimetype of a file
     *
     * @param string $path path to a file
     * @access public
     * @return string file Mimetype
     */
    public function getMime($path);
}
