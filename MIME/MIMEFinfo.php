<?php

/**
 * This file is part of the Stream\Filesystem Package
 *
 * (c) Thomas Appel <mail@thomas-appel.com
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem\MIME;

/**
 * FinfoMimeSniffer
 *
 * @uses InterfaceMimeSniffer
 * @package Stream\Filesystem\Mime
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class MIMEFinfo implements InterfaceMimeSniffer
{
    /**
     * {@inheritDoc}
     */
    public function getMime($path)
    {
        return finfo_file(finfo_open(FILEINFO_MIME_TYPE), $path);
    }
}
