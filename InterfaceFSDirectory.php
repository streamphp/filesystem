<?php

/**
 * This file is part of the Stream\Filesystem Package
 *
 * (c) Thomas Appel <mail@thomas-appel.com
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem;

/**
 * FSDirectory
 * @interface
 * @package
 * @license
 */
interface InterfaceFSDirectory extends InterfaceFSObject
{

    /**
     * create an new, empty file within the current directory
     *
     * @param string $filename filename
     * @param int  $mode  Octal access representation
     * @access public
     * @return mixed
     */
    public function touch($filename, $perm);

    /**
     * create a new die within the current directory
     *
     * @param string|null  $name null or name of the new directory, can be a multidimensional
     *  relative path
     * @param int          $mode  Octal access representation
     * @param boolean      $recursive set this to true, if you want to realise a recursive path
     * @param string       $placeholder default directory name if $name is omitted
     * @access public
     * @return boolean
     */
    public function mkdir($name = null, $permission = null, $recursive = false, $placeholder = 'unnamed folder');

    /**
     * filter
     *
     * @param mixed $ignore
     * @param array $exclude
     * @access public
     * @return mixed
     */
    public function filter($ignore = null, array $exclude = null);

    /**
     * recursive
     *
     * @access public
     * @return mixed
     */
    public function recursive();

    /**
     * get
     *
     * @param mixed $path
     * @access public
     * @return mixed
     */
    public function get($path);

    /**
     * isEmpty
     *
     * @access public
     * @return mixed
     */
    public function isEmpty();

    /**
     * getLastCreated
     *
     * @access public
     * @return mixed
     */
    public function getLastCreated();

    /**
     * purgeFiles
     *
     * @param mixed $recursive
     * @access public
     * @return mixed
     */
    public function purgeFiles($recursive = false);
}
