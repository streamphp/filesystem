<?php

/**
 * This file is part of the Stream\Filesystem Package
 *
 * (c) Thomas Appel <mail@thomas-appel.com
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem;

use ArrayAccess;
use Iterator;
use Stream\Filesystem\Exception\FSIOException;
use Stream\Common\Interfaces\InterfaceToArray;

/**
 * FSDirectory
 *
 * @uses FSObject
 * @implements \ArrayAccess
 * @package Stream\Filesystem
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 * @version 1.0
 *
 * @property-read string  $name   Directory name
 * @property-read string  $path   Directory path
 * @property-read string  $uid    Userid
 * @property-read string  $gid    Groupid
 * @property-read string  $owner  Owner
 * @property-read string  $group  Group
 * @property-read string  $perm   Permission level
 */
class FSDirectory extends AbstractFSObject implements ArrayAccess, Iterator, InterfaceToArray, InterfaceFSDirectory
{
    /**
     * Ignore this types by default
     * @var Array
     */
    protected static $isdot = ['.', '..'];

    /**
     * directories
     *
     * a list of subdirectories
     *
     * @var Array
     * @access protected
     */
    protected $directories = [];

    /**
     * files
     *
     * a list of files
     *
     * @var Array
     * @access protected
     */
    protected $files = [];

    /**
     * files and directories that where created on this class instance
     *
     * @var Array
     * @access protected
     */
    protected $created = [];

    /**
     * Pool of initialized FSObject Objects
     *
     * @var Mixed
     * @access protected
     */
    protected $cached = [];

    /**
     * position
     *
     * @var int
     */
    protected $position;

    /**
     * paths
     *
     * @var array
     */
    protected $paths;

    /**
     * handle
     *
     * @var mixed
     */
    private $handle;

    /**
     * tempfilter
     *
     * @var array
     */
    private $tempfilter;

    /**
     * recursive
     *
     * @var boolean
     */
    private $recursive = false;

    /**
     * listed
     *
     * @var mixed
     */
    private $listed = false;

    /**
     * __construct
     *
     * @param string  $path Path of this directory
     * @param integer $perm octal permission, default `0775`.
     * @access public
     */
    public function __construct($path, $perm = 0775)
    {
        parent::__construct($path, $perm);
        $this->position = 0;
        $this->setTempFilterArray();
        //$this->getContents();
    }

    /**
     * __destruct
     *
     */
    public function __destruct()
    {
        $this->closeDirhandle();
    }

    /**
     * List directory and file structure as array
     *
     * @param string|null $ignore  Regular expression for ignoring files or directoies
     * @param array|null  $exclude An array containign absolute paths that should be
     *  exluded
     * @access public
     *
     * @return Array Directory structure as associatove array
     */
    public function toArray()
    {
        $this->ensureAttributes();
        $this->ensureListed();

        $out = ['attributes' => $this->attributes];
        foreach (['directories', 'files'] as $type) {
            $this->contentsToArray($type, $out);
        }
        $this->setTempFilterArray();
        return $out;
    }

    /**
     * filter
     *
     * @param  string|null                   $ignore  regular expression
     * @param  array|null                    $exclude array of file paths
     * @return Stream\Filesystem\FSDirectory
     */
    public function filter($ignore = null, array $exclude = null)
    {
        $this->tempfilter = [$ignore, $exclude];
        return $this;
    }

    /**
     * recursive
     *
     */
    public function recursive()
    {
        $this->recursive = true;
        $this->ensureListed();

        if ($args = func_get_args()) {

            if (isset($args[0])) {
                $fn = array_shift($args);
                call_user_func_array([$this, $fn], $args);
                $this->recursive = false;
            }
        }
        return $this;
    }

    /**
     * current
     * @see \Iterator#current()
     */
    public function current()
    {
        $paths = $this->getPaths();
        return $this[$paths[$this->position]];
    }

    /**
     * key
     * @see \Iterator#key()
     */
    public function key()
    {
        $paths = $this->getPaths();
        return $paths[$this->position];
    }

    /**
     * next
     * @see \Iterator#next()
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * rewind
     * @see \Iterator#rewind()
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * valid
     * @see \Iterator#valid()
     */
    public function valid()
    {
        $paths = $this->getPaths();
        return isset($paths[$this->position]);
    }

    /**
     * Move the directory to a different location
     *
     * @see Stream\Filesystem\FSObject::move
     *
     * @param string $location new location of the directory
     * @param boolen $enum     directory should be renamed using a numbered sequence, default `true`
     *  if destination already exists
     * @param integer $enumBase   startvalue of the enum sequence, default `1`
     * @param String  $enumPrefix enumeration prefix (which defaults to `' copy '`)
     * @access public
     * @return Boolean
     */
    public function move($location, $enum = true, $enumBase = 1, $enumPrefix = 'copy')
    {
        if ($newName = $this->moveDir(static::normalizePath($location), $enum, $enumBase, $enumPrefix)) {

            $this->directories = [];
            $this->files = [];
            $this->getContents();

            return $newName;
        }

        return false;
    }

    /**
     * copy
     *
     * @param mixed $location
     * @param mixed $enum
     * @param int $enumBase
     * @param string $enumPrefix
     * @access public
     * @return mixed
     */
    public function copy($location = null, $enum = true, $enumBase = 1, $enumPrefix = 'copy')
    {
        if (is_null($location)) {
            $location = $this->path;
        }
        if ($copyName = $this->moveDir(static::normalizePath($location), $enum, $enumBase, $enumPrefix, true)) {

            $this->directories = [];
            $this->files = [];
            $this->getContents();

            return $copyName;
        }

        return false;
    }

    /**
     * Check if the directory exists.
     *
     * Will typically return false after a `FSDirectory::remove` call
     *
     * @access public
     * @return Boolean
     */
    public function exists()
    {
        return is_dir($this->path);
    }

    /**
     * get
     *
     * @param Mixed $path
     * @access public
     * @return void
     */
    public function get($path)
    {
        $fsObject = null;
        $bits = $this->explodePath(static::normalizePath($path));

        while (count($bits) > 0) {
            $path = array_shift($bits);

            if (is_null($fsObject)) {
                if (!isset($this[$path])) {
                    break;
                }
                $fsObject =& $this[$path];
                continue;
            }

            if (!isset($fsObject[$path])) {
                $fsObject = null;
                break;
            }
            $fsObject =& $fsObject[$path];
        }
        return $fsObject;
    }

    protected function explodePath($path)
    {
        return preg_split('%\/%', $path, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * trimPath
     *
     * @param mixed $path
     * @access protected
     * @return mixed
     */
    protected function trimPath($path)
    {
        if (static::isRelativePath($path)) {
            return $path;
        }
        return false !== strcmp($path, $this->path) ? substr($path, strlen($this->path) + 1) : null;
    }

    /**
     * Recursive remove a directory
     *
     * This will recursively call remove on all FSObjects in `directories`
     * and `files`
     *
     * @access public
     * @return Boolean
     */
    public function remove()
    {
        foreach ($this->getPaths() as $item) {
            $fsObject = $this[$item];
            $fsObject->remove();
        }
        return rmdir($this->path);
    }

    /**
     * isEmpty
     * @return boolean
     */
    public function isEmpty()
    {
        $this->ensureListed();
        return empty($this->directories) && empty($this->files);
    }

    /**
     * Implementation of FSObject::isFile()
     *
     * @access public
     * @return Boolean false
     */
    public function isFile()
    {
        return false;
    }

    /**
     * Implementation of FSObject::isDir()
     *
     * @access public
     * @return Boolean
     */
    public function isDir()
    {
        return $this->exists();
    }

    /**
     * Creates a new Directory
     *
     * @see http://www.php.net/manual/en/arrayaccess.offsetset.php PHP documentation reference.
     *
     * @param Mixed $offset directory or filename
     * @param Mixed $value
     * @access public
     * @return void
     *
     * @throws FSIOException
     */
    public function offsetSet($offset, $value)
    {
        $value = basename($value);

        if (!is_dir($path = $this->getPathname($value))) {
            if ($this->mkdir($value)) {
                $this->directories[$value] = $path;
            }
        }
    }

    /**
     *
     * This will remove a file or directory
     *
     * @see http://www.php.net/manual/en/arrayaccess.offsetunset.php PHP documentation reference.
     * @see \ArrayAccess::offsetUnset()
     * @param String $offset directory or filename
     * @access public
     * @return void
     */
    public function offsetUnset($offset)
    {
        if ($type = $this->getOffsetType($offset)) {
            $this[$offset]->remove();
            $this->paths = null;
            unset($this->{$type}[$offset]);
        }
    }

    /**
     * Get a File or Directory by name
     *
     * @see http://www.php.net/manual/en/arrayaccess.offsetget.php PHP documentation reference.
     * @see \ArrayAccess::offsetGet()
     *
     * @param String $offset
     * @access public
     * @return FSObject or null
     */
    public function offsetGet($offset)
    {
        if ($type = $this->getOffsetType($offset)) {
            $fsObject = $this->getByType($type, $offset);
            return $fsObject;
        }
    }

    /**
     * check if a file or directory exists
     *
     * @see http://www.php.net/manual/en/arrayaccess.offsetexists.php for PHP documentation reference.
     * @see \ArrayAccess::offsetExists()
     * @param String $offset directory or filename
     * @access public
     * @return booelan
     */
    public function offsetExists($offset)
    {
        if ($type = $this->getOffsetType($offset)) {
            if (!file_exists($this->getPathname($offset))) {
                unset($this->{$type}[$offset]);
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Create a file within this directory
     *
     * @param String       $filename filename
     * @param integer|0644 $perm     octal file permission
     * @access public
     * @return boolean
     */
    public function touch($filename, $perm = 0644)
    {
        $filename = basename($filename);
        $path = $this->getPathname($filename);

        if (!file_exists($path)) {
            $this->convertPermission($perm);
            touch($path);
            $this->files[$filename] = $path;
            return true;
        }

        return false;
    }

    /**
     * Create a new directory within this directory
     *
     * @param string|null  $name        relative path to be created
     * @param integer|null $permission  directory permission
     * @param boolean      $recursive   create recursively, defaults to `false`
     * @param string       $placeholder placeholder name when $name is omitted.
     *  Defaults to `'unnamed folder'`
     * @access public
     * @return string|boolean
     *
     * @throws FSIOException
     */
    public function mkdir($name = null, $permission = null, $recursive = false, $placeholder = 'unnamed folder')
    {
        $originalName = $name;

        if (is_null($name)) {
            $path = $this->enum($this->getPathName($placeholder), 2, ' ', false);
            $name = basename($path);
        } else {
            $path = $this->getPathName($name);
        }

        $perm = is_null($permission) ? $this->perm : $permission;
        $this->convertPermission($perm);

        // do not attempt to mkdir on an existsing directory:
        if (is_dir($path)) {
            throw new FSIOException(sprintf("Directory %s already exists", $path));
        }

        if (!$recursive && !is_dir(dirname($path))) {
            throw new FSIOException(
                sprintf("cannot create direcoty %s [recursive: %s]", $path, $recursive ? 'true' : 'false')
            );
        }

        if (!@mkdir($path, $perm, $recursive)) {
            return false;
        }

        if ($recursive) {
            $bits = preg_split('~\/~', static::normalizePath($name), -1, PREG_SPLIT_NO_EMPTY);
            $name = $bits[0];
            $path = $this->getPathname($bits[0]);
        }

        $this->directories[$name] = $path;
        natsort($this->directories);

        return is_null($originalName) ? $path : true;
    }

    protected function getPathName($name)
    {
        return sprintf('%s%s%s', $this->path, static::DIRECTORY_SEPARATOR, $name);
    }

    /**
     * getLastCreated
     *
     * @access public
     * @return void
     */
    public function getLastCreated()
    {
        $lastObj = end($this->created);
        reset($this->created);
        return $lastObj;
    }

    /**
     * ensures a file or directory exists
     *
     * Attempts to create a new directory or file with the given relative path is this
     * directory does not exist.
     *
     * @param string  $relPath directory path relative to `$this->path`
     * @param boolean $file    weather file or directory
     * @access public
     * @return Boolean
     */
    public function ensureExists($relPath, $file = false)
    {
        $path = $this->getPathname(static::normalizePath($relPath));

        if ($file === false && !is_dir($path)) {
            return $this->mkdir($relPath, $this->perm, true);
        } elseif ($file && !is_file($path)) {

            $relDir = dirname($relPath);
            $fileName = basename($relPath);

            if (!is_dir(dirname($path))) {

                if ($this->mkdir($relDir, $this->perm, true)) {
                    return $this->get($relDir)->touch($fileName, $this->perm) !== false;
                }

            } else {
                return $this->touch($fileName, $this->perm) !== false;
            }

            return false;
        }

        return true;
    }

    /**
     * remove all files without removing directories
     *
     * @param boolean $recursive recursively remove files, default `false`
     * @access public
     * @return void
     */
    public function purgeFiles($recursive = false)
    {
        $this->ensureListed();
        foreach ($this->files as $file => $path) {
            $this[$file]->remove();
        }

        if ($recursive) {
            foreach ($this->directories as $dir => $path) {
                $this[$dir]->purgeFiles($recursive);
            }
        }
    }

    /**
     * Rename this directory
     *
     * @see Stream\Filesystem\FSObject::rename()
     *
     * @param  Mixed $name
     * @access public
     * @return Boolean
     */
    public function rename($name)
    {
        $path = static::isRelativePath($name) ? dirname($this->path) .
            self::DIRECTORY_SEPARATOR . $name : realpath($name);

        if (is_dir($path)) {
            throw new FSIOException(sprintf("cannot rename %s to %s: %s already exists", $this->path, $path, $path));
        }
        return $this->move(static::normalizePath($path), false);
    }

    /**
     * Change ownership of this directory.
     *
     * @param Mixed   $ownerID   set owner id
     * @param boolean $recursive recursively chown
     * @param boolean $clearstat clear filestat after operation
     * @access public
     * @return void
     */
    public function chown($ownerID, $clearstat = true)
    {
        if (parent::chown($ownerID, $clearstat)) {
            if ($this->recursive) {
                if ($this->changeOwnerShipRecursive($this->directories, $ownerID, 'doChown', $clearstat) &&
                    $this->changeOwnerShipRecursive($this->files, $ownerID, 'doChown', $clearstat)) {
                    return true;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Change group of directory
     *
     * @see Stream\Filesystem\FSObject::chgrp()
     *
     * @param Mixed   $groupID   set group id
     * @param boolean $recursive recursively chgrp, default `false`
     * @param boolean $clearstat clear filestat after operation, default `true`
     * @access public
     * @return boolean
     */
    public function chgrp($groupID, $clearstat = true)
    {
        if (parent::chgrp($groupID, $clearstat)) {

            if ($this->recursive) {

                if ($this->changeOwnerShipRecursive($this->directories, $groupID, 'doChgrp', $clearstat) &&
                    $this->changeOwnerShipRecursive($this->files, $groupID, 'doChgrp', $clearstat)
                ) {
                    return true;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * change directory permsission.
     *
     * @see FSObject::permission()
     * @see FSObject::chmod()
     *
     * @param Mixed   $mode      octal permission mode
     * @param boolean $recursive recursively chown, default `false`
     * @param boolean $clearstat clear filestat after operation, default `true`
     * @access public
     * @return Boolean
     */
    public function chmod($mode, $clearstat = true)
    {
        return $this->permission($mode, $clearstat);
    }

    /**
     * change directory permsission.
     *
     * Returns current permission setting if $mode is omitted.
     *
     * @param integer|null $mode      octal permission mode
     * @param boolean      $recursive recursively chown, default `false`
     * @param boolean      $clearstat clear filestat after operation, default
     *  `true`
     * @access public
     * @return Boolean
     */
    public function permission($mode = null, $clearstat = true)
    {
        if (!$this->recursive || is_null($mode)) {
            return parent::permission($mode, $clearstat);
        }

        if (parent::permission($mode, $clearstat) &&
            $this->changeOwnerShipRecursive($this->directories, $mode, 'chmod', $clearstat) &&
            $this->changeOwnerShipRecursive($this->files, $mode, 'chmod', $clearstat)
        ) {
            return true;
        }

        return false;
    }

    /**
     * Get an instance of FSObject (FSDirectory or FSFile) depending  on `$type`
     *
     * @param string $type `directories` or `files`
     * @param string $path file or directory path
     * @access protected
     * @return FSObject
     */
    protected function getByType($type, $path)
    {

        $base = basename($path);
        $path = $this->getPathname($path);

        if (isset($this->cached[$base])) {
            return $this->cached[$base];
        }

        $object = $type === 'directories' ? new FSDirectory($path) : new FSFile($path);
        $this->{$type}[$base] = $path;
        $this->logCreated($object);

        return $object;
    }

    /**
     * Get attributes of a directory
     *
     * @param string $file directory path
     * @access protected
     * @return array
     */
    protected function getAttributes($file)
    {
        $uid = @fileowner($file);
        $gid = @filegroup($file);

        return [
            'name'  => basename($file),
            'path'  => $file,
            'uid'   => $uid,
            'gid'   => $gid,
            'owner' => $this->getUserName($uid),
            'group' => $this->getGroupName($gid),
            'perm'  => substr(sprintf('%o', fileperms($file)), -4),
        ];
    }

    /**
     * Iterate over the contents of the current directory.
     * Will populate $this->directories and $this->files.
     *
     * @access protected
     * @return void
     */
    protected function getContents($force = false)
    {
        //if ($this->listed) {
        //    return;
        //}
        //if (!$force && (!empty($this->directories) || !empty($this->files))) {
        //    return;
        //}
        $this->paths = null;

        if (!$this->handle = opendir($this->path)) {
            throw new FSIOException('cannot iterate over directory');
        }

        while (($item = readdir($this->handle)) !== false) {

            if ($this->isDot($item)) {
                continue;
            }

            $bname = $item;
            $item = $this->getPathname($item);

            if (is_dir($item)) {
                $this->directories[$bname] = $item;
                continue;
            }

            if (is_file($item)) {
                $this->files[$bname] = $item;
                continue;
            }
        }

        $this->closeDirhandle();
        $this->listed = true;
    }

    /**
     * enumerate a filename
     *
     * @see Stream\Filesystem\FSObject::enum()
     */
    protected function enum($file, $start, $prefix = null, $pad = true)
    {
        if (!is_dir($file)) {
            return $file;
        }

        $prefix = is_null($prefix) ?  $prefix : ($pad ? str_pad($prefix, strlen($prefix) + 2, ' ', STR_PAD_BOTH) : $prefix);

        $i = $start;
        while (is_dir(sprintf("%s%s%d", $file, $prefix, $i))) {
            $i++;
        }
        return sprintf("%s%s%d", $file, $prefix, $i);
    }

    /**
     * @see Stream\Filesystem\FSObject::create()
     *
     * @param string $path directory path
     * @access protected
     * @return Boolean
     */
    protected function create($path)
    {
        if (is_dir($path)) {
            return true;
        }

        throw new FSIOException(sprintf("%s: no such directory", $path));

        //if (mkdir($path, $perm, true)) {
        //    $this->logCreated($this);
        //    return true;
        //}
        //return false;
    }

    /**
     * compares string against self::$isdot.
     *
     * @param Mixed $item
     * @access private
     * @return void
     */
    private function isDot($item)
    {
        return in_array($item, static::$isdot);
    }

    /**
     * Convert the directory structure to an array.
     *
     * This is called by FSDirectory::toArray()
     *
     * @param string $type 'directories' or 'files'
     * @param array  $out  output array
     * @access private
     * @return void
     */
    private function contentsToArray($type, array &$out)
    {
        if (!empty($this->{$type})) {

            list($ignore, $exclude) = $this->tempfilter;
            $out[$type] = [];

            foreach ($this->{$type} as $item) {

                if ($this->isIgnored(basename($item), $ignore)) {
                    continue;
                }

                if ($this->isExcluded($item, $exclude)) {
                    continue;
                }

                $object = $this[basename($item)];

                if ($object->isDir()) {
                    $object->filter($ignore, $exclude);
                }

                $out[$type][] = $object->toArray();
            }
        }
    }

    /**
     * skip file if its path is within $dirs
     *
     * @param string $dir       file path
     * @param array  $dirs|null array of file paths
     * @access private
     * @return boolean
     */
    private function isExcluded($dir, $dirs = null)
    {
        if (!is_array($dirs) || empty($dirs)) {
            return false;
        }

        return in_array(str_replace('\\', '/', $dir), $dirs);
    }

    /**
     * skip file if its name matched a pattern defined in `$regexp`
     *
     * @param string $file        filepath
     * @param string $regexp|null regular expression
     * @access private
     * @return void
     */
    private function isIgnored($file, $regexp = null)
    {
        if (is_null($regexp)) {
            return false;
        }

        return preg_match($regexp, $file);
    }

    /**
     * Is called each time a new FSObject is created.
     *
     * Logs the filename to an internal cache storage.
     * The key is represented as the filemtime.
     *
     * @param FSObject $obj a referent to a FSObject
     * @access private
     * @return void
     */
    private function logCreated(AbstractFSObject &$obj)
    {
        $this->cached[$obj->name] = &$obj;
        $this->created[filemtime($obj->path)] = $obj->name;
        $this->paths = null;
    }

    /**
     * Recursively call chmod|chown|chgrp on files or directories.
     *
     * Files or directories are defiend in $objects
     *
     * @param array   $objects   an array containing file paths
     * @param Mixed   $value     value to be set on chmod|chown|chgrp
     * @param string  $type      chmod|chown|chgrp
     * @param boolean $clearstat clear filestat cache
     * @access private
     * @return Boolean
     */
    private function changeOwnerShipRecursive($objects, $value, $type = 'chmod', $clearstat = true)
    {
        $fails = false;

        if (!is_array($objects)) {
            return true;
        }

        foreach ($objects as $item) {

            $object = $this[basename($item)];

            if ($object->isDir()) {
                $object->recursive();
            }

            if (!$object->{$type}($value, $clearstat)) {
                $fails = true;
                break;
            }
        }

        $this->recursive = false;

        return !$fails;
    }

    /**
     * determine if the contents array is `$this->files` or `$this->directories`
     * and if the given offset is set
     *
     * @param String $offset directory or filename
     * @access private
     * @return Mixed null if nothing is found or String ['directories' | 'files']
     */
    private function getOffsetType($offset)
    {
        $this->ensureListed();
        $path = $this->getPathname($offset);
        return isset($this->directories[$offset]) ? 'directories' : (isset($this->files[$offset]) ? 'files' : false);
    }

    /**
     * ensureListed
     *
     * @access protected
     * @return mixed
     */
    protected function ensureListed()
    {
        if ($this->listed) {
            return;
        }
        $this->getContents();
    }

    /**
     * move ad directory to a new location
     *
     * This is called by FSDirectory::move()
     *
     * @param string  $location
     * @param boolean $enum
     * @param integer $enumBase
     * @param string  $enumPrefix
     * @access private
     * @return boolean
     */
    private function moveDir($location, $enum, $enumBase, $enumPrefix, $copy = false)
    {

        $this->paths = null;
        $canMove = false;
        $name = $this->name;

        $fn = $copy ? 'copy' : 'rename';


        //determine weather to move inside the location or if location is the destination file:
        if (!preg_match('/(\\/)$/', $location)) {
            $name = basename($location);
            $location = dirname($location);
        }

        if (!is_dir($location)) {
            return false;
        }

        // move this here
        $nbase = sprintf('%s%s', rtrim($location, self::DIRECTORY_SEPARATOR), self::DIRECTORY_SEPARATOR);
        $npath = $enum ?
            sprintf('%s%s', $nbase, basename($this->enum(sprintf('%s%s', $nbase, $name), $enumBase, $enumPrefix))) :
            sprintf('%s%s', $nbase, $name);
        $canMove = true;

        $fn = $copy ? [$this, 'copyDir'] : 'rename';
        $args = [$this->path, $npath];

        if ($canMove && (call_user_func_array($fn, $args))) {
            if (!$copy) {
                $this->setAttributes($npath);
            }
            return $npath;
        }
        return false;
    }

    /**
     * copyDir
     *
     * @param mixed $source
     * @param mixed $dest
     * @access private
     * @return mixed
     */
    private function copyDir($source, $dest)
    {
        $perm = $this->perm;
        $this->convertPermission($perm);
        mkdir($dest, $perm);

        $dir = new self($dest);

        foreach ($this as $path => $item) {
            if ($item->isFile()) {
                if (!@copy($item->path, $dest . static::DIRECTORY_SEPARATOR . $path)) {
                    return false;
                }
                continue;
            }

            if (!$item->copy($dest . static::DIRECTORY_SEPARATOR . $path)) {
                return false;
            }
        }
        return true;
    }

    /**
     * within
     *
     * @param mixed $path
     * @access private
     * @return boolean
     */
    private function within($path)
    {
        return false !== strpos($path, $this->path);
    }

    /**
     * getPaths
     *
     * @access private
     * @return mixed
     */
    private function getPaths()
    {
        if (is_null($this->paths)) {
            $this->ensureListed();
            $this->paths = array_keys(array_merge($this->directories, $this->files));
        }

        return $this->paths;
    }

    /**
     * closeDirhandle
     *
     * @access private
     * @return mixed
     */
    private function closeDirhandle()
    {
        if (is_resource($this->handle)) {
            closedir($this->handle);
        }
    }

    /**
     * setTempFilterArray
     *
     * @access private
     * @return mixed
     */
    private function setTempFilterArray()
    {
        $this->tempfilter = [null, null];
    }
}
