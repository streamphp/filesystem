<?php

namespace Stream\Filesystem;

interface InterfaceFSObject
{
    /**
     * Object is a file
     *
     * @access public
     * @return boolean
     */
    public function isFile();

    /**
     * Object is a directory
     *
     * @access public
     * @return boolean
     */
    public function isDir();

    /**
     * path exists
     *
     * @access public
     * @return boolean
     */
    public function exists();

    /**
     * get original attributes
     *
     * @access public
     * @return array
     */
    public function getOriginalAttributes();

    /**
     * rename a file or directoy
     *
     * @param string $name new naae
     * @access public
     * @return mixed
     */
    public function rename($name);

    /**
     * remove a file or directory
     *
     * @access public
     * @return boolean
     */
    public function remove();

    /**
     * move a file or directory to a different location
     *
     * @param string  $location new location
     * @param boolean $enum
     * @param int     $enumBase
     * @param string  $enumPrefix
     * @access public
     * @return boolean
     */
    public function move($location, $enum = true, $enumBase = 1, $enumPrefix = ' copy ');

    /**
     * Change or retreive permsission
     *
     * Returns current permission setting if $mode is omitted.
     *
     * @param  int  $mode  Octal access representation
     * @access public
     * @return boolean|integer true if permission was altererd or current
     *  permission ($this->perm).
     *
     *  @throws FSIOException
     */
    public function permission($mode = null, $clearstat = true);

    /**
     * change permission settings
     *
     * @param int     $mode  Octal access representation
     * @param boolean $clearstat
     * @access public
     * @return boolean
     */
    public function chmod($mode, $clearstat = true);

    /**
     * change ownership settings
     *
     * @param Mixed   $ownerID   set owner id
     * @param boolean $clearstat
     * @access public
     * @return boolean
     */
    public function chown($ownerID, $clearstat = true);

    /**
     * change group
     *
     * @param Mixed $groupID
     * @param Mixed $clearstat
     * @access public
     * @return booelan
     */
    public function chgrp($groupID, $clearstat = true);

    /**
     * determine if a given path is relative
     *
     * @param string $path the path to be checked
     * @access public
     * @return booelan
     */
    public static function isRelativePath($path);
}
