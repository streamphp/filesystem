<?php

/**
 * This File is part of the Stream\Filesystem package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem;

use Stream\Filesystem\Exception\FSIOException;

/**
 * Class: Filesystem
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class Filesystem
{
    /**
     * fsobjects
     *
     * @var array
     */
    protected $fsobjects = [];

    /**
     * currentPath
     *
     * @var string
     */
    protected $currentPath;

    /**
     * methods
     *
     * @var mixed
     */
    protected $directoryMethods;

    /**
     * __construct
     *
     * @param mixed $path
     */
    public function __construct($path = null)
    {
        $this->currentPath = $this->normalizePath(is_null($path) ? getcwd() : $this->getRealPath($path));
        $this->getFSObject($this->currentPath, true);
    }

    /**
     * __call
     *
     * @param string $method
     * @param array  $arguments
     */
    public function __call($method, $arguments = null)
    {
        $object = $this->getFSObject($this->getRealPath($this->currentPath));
        return call_user_func_array([$object, $method], $arguments);
    }


    /**
     * __callStatic
     *
     * @param mixed $method
     * @param array $arguments method arguments
     * @access public
     * @return mixed
     */
    public static function __callStatic($method, $arguments = null)
    {
        return call_user_func_array('Stream\Filesystem\FSDirectory::' . $method, $arguments);
    }

    /**
     * get
     *
     * @param mixed $path
     * @access public
     * @return mixed
     */
    public function get($path = null)
    {
        if (is_null($path)) {
            return $this->getFSObject($this->currentPath);
        }

        return $this->getFSObject($this->getRealPath($path));
    }

    /**
     * getRealPath
     *
     * @param  mixed             $path
     * @return mixed|string|null
     */
    public function getRealPath($path)
    {
        if ($this->isRelativePath($path)) {
            if ($realpath = realpath($path)) {
                return $this->normalizePath($realpath);
            }
            return sprintf("%s/%s", $this->getCurrentPath(), $this->normalizePath($path));
        }

        if ($this->isAbsolutePath($path)) {
            return $this->normalizePath($path);
        }
    }

    /**
     * getCurrentPath
     *
     * @access public
     * @return mixed
     */
    public function getCurrentPath()
    {
        if (!isset($this->currentPath)) {
            $this->currentPath = $this->normalizePath(getcwd());
        }
        return $this->currentPath;
    }

    /**
     * isAbsolutePath
     *
     * @param mixed $path
     */
    public function isAbsolutePath($path)
    {
        return file_exists($path);
    }

    /**
     * symlink
     *
     * @param mixed $source
     * @param mixed $target
     */
    public function symlink($source, $target)
    {
        $this->getPaths($source, $target);

        if (!@`ln -s $source $target`) {
            return $this->copy($source, $target);
        }

        return false;
    }

    /**
     * isDir
     *
     * @param mixed $path
     * @access public
     * @return boolean
     */
    public function isDir($path = null)
    {
        if (is_null($path)) {
            return $this->__call('isDir');
        }
        return is_dir($path);
    }

    /**
     * isFile
     *
     * @param mixed $path
     * @access public
     * @return boolean
     */
    public function isFile($path = null)
    {
        if (is_null($path)) {
            return $this->__call('isFile');
        }
        return is_file($path);
    }

    /**
     * move
     *
     * @param mixed $currentName
     * @param mixed $newName
     * @access public
     * @return mixed
     */
    public function move($currentName, $newName = null)
    {
        $this->getPaths($currentName, $newName);
        return $this->getFSObject($currentName)->move($newName);
    }

    /**
     * copy
     *
     * @param mixed $currentName
     * @param mixed $newName
     * @access public
     * @return mixed
     */
    public function copy($currentName = null, $newName = null)
    {
        $currentName = is_null($currentName) ? $this->getCurrentPath() : $currentName;
        $this->getPaths($currentName, $newName);
        return $this->getFSObject($currentName)->copy($newName);
    }

    /**
     * rename
     *
     * @param mixed $currentName
     * @param mixed $newName
     * @access public
     * @return mixed
     */
    public function rename($currentName, $newName = null)
    {
        if (is_null($newName)) {
            return $this->__call('rename', func_get_args());
        }
        return $this->getFSObject($this->getRealPath($currentName))->rename($newName);
    }

    /**
     * chmod
     *
     * @param mixed $path
     * @param mixed $perm
     * @access public
     * @return mixed
     */
    public function chmod($path = null, $perm = null, $recursive = false)
    {
        return $this->execPermissionMethod('chmod', $perm, $path, $recursive);
    }

    /**
     * normalizePath
     *
     * @param mixed $path
     * @access public
     * @return mixed
     */
    public function normalizePath($path)
    {
        return FSDirectory::normalizePath($path);
    }

    /**
     * isRelativePath
     *
     * @param mixed $path
     * @access public
     * @return mixed
     */
    public function isRelativePath($path)
    {
        return FSDirectory::isRelativePath($path);
    }

    /**
     * execPermissionMethod
     *
     * @param mixed $method
     * @param mixed $perm
     * @param mixed $path
     * @access private
     * @return mixed
     */
    private function execPermissionMethod($method, $perm = null, $path = null, $recursive = false)
    {
        if (is_null($path)) {
            $path = $this->getCurrentPath();
        } else {
            $path = $this->getRealPath($path);
        }
        $object = $this->get($path);
        if ($recursive && $object->isDir()) {
            $object->recursive();
        }
        return call_user_func_array([$object, $method], [$perm, true]);
    }


    /**
     * getPaths
     *
     * @param mixed $currentName
     * @param mixed $newName
     * @access protected
     * @return void
     */
    protected function getPaths(&$currentName, &$newName = null)
    {
        $currentName = $this->normalizePath($currentName);
        $oldCurrent = $currentName;

        if (is_null($newName)) {
            $newName = $currentName;
            $currentName = $this->getCurrentPath();
        } else {
            $newName     = $this->normalizePath($newName);
        }

        $currentName = $this->getRealPath($currentName);

        if (is_file($file = sprintf('%s%s%s', $currentName, FSDirectory::DIRECTORY_SEPARATOR, $oldCurrent))) {
            $currentName = $file;
        }

        $newName = $this->isRelativePath($newName) ?
            sprintf('%s%s%s', $this->getCurrentPath(), FSDirectory::DIRECTORY_SEPARATOR, $newName) :
            $newName;
    }

    /**
     * setFSObject
     *
     * @param  string $path
     * @throws Stream\Filesystem\Exception\FSIOException
     * @return AbstractFSObject
     */
    protected function getFSObject($path, $initial = false)
    {
        if (!isset($this->fsobjects[$path])) {
            if ($this->isfile($path)) {
                if ($initial) {
                    throw new FSIOException('Cannot initialize filesystem with a file url');
                }
                $fsobject = $this->createFileObject($path);
            } else {
                $fsobject = $this->createDirectoryObject($path);
            }
            $this->fsobjects[$path] =& $fsobject;
        }

        return $this->fsobjects[$path];
    }

    /**
     * createFileObject
     *
     * @param mixed $path
     */
    protected function createFileObject($path = null)
    {
        if (!is_file($path)) {
            throw new FSIOException(sprintf('%s: no such file', $path));
        }
        return new FSFile($path);
    }

    /**
     * createDirectoryObject
     *
     * @param mixed $path
     * @access protected
     * @return mixed
     */
    protected function createDirectoryObject($path = null)
    {
        if (!is_dir($path)) {
            throw new FSIOException(sprintf('%s: no such direcory', $path));
        }
        return new FSDirectory($path);
    }
}
