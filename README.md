# Stream Filesystem
## Filesystem libraray for streamphp
[![Build Status](https://api.travis-ci.org/streamphp/filesystem.png?branch=master)](https://travis-ci.org/streamphp/filesystem)


### Usage


#### Examples

##### Using the Filesystem class

```php

<?php
use Stream\Filesystem\Filesystem;

$fsys = new Filesystem('/path/to/dir');

$fsys->mkdir('newdir', 0755);

$fsys->touch('file.txt', 0665);

$fsys->copy('file.txt'); // => file copy 1.txt
$fsys->copy('file.txt', 'file2.txt');

$fsys->get();            // => FSDirectory /path/to/dir
$fsys->get('newdir');    // => FSDirectory /path/to/dir/newdir

$fsys->get('file.txt');  // => FSFile


```

Calling the `Filesystem` constructor without a path will create a new `Filesystem` object using the current working directory.
Calling the `Filesystem` constructor with a none existsing path or a file url will throw a `FSIOException`.

##### Create fileroot on which you want to operate

```php

<?php
use Stream\Filesystem\Filesystem;
use Stream\Filesystem\FSDirectory;

$fsys = new Filesystem('/path/to/rootdir');
$root = $fsys->get();
// or
$root = new FSDirectory('/path/to/rootdir');

```


##### getting attributes

```php

echo $root->path;    // '/path/to/rootdir'
echo $root->perm;    // '0755'
echo $root->isDir(); // true

```

##### create a directory …

```php

if ($root->mkdir('newDir', 0775)) {
		…
// All arguments on mkdir are optional, 
// if the first argument is omitted, mkdir tries to create a directory that 
// is called `unnamed folder` by default, though the name will be enumerated 
// in case it already exists. 
	
 // this will create two directories `unnamed folder` and `unnamed folder 2`
 $root->mkdir() && $root->mkdir();	
		…
	
// change the dafault placeholder name
$root->mkdir(null, null, fase, 'unnamed directory');	
		…

 // recursive: 
 $root->mkdir('path/subpath)', 755, true);	
		…

```

##### … and operate on it

```php
	…
		
	$dir = $root['newDir'];
	echo $dir->isDir(); // true
		
	// change owner ship, `true` as pass second parameter for recursive ownership change
	// chgrp and chmod work in the same way
	$dir->chown('newowner', true);
	$dir->chgrp('newgroup', true);
	$dir->chmod(777, true);
		
	// recursively delete a directory:		
	$dir->remove();
	// or
	$root['newDir']->remove();
}
```

##### create a file…
```php
	$root->touch('file.txt', 0655);
	$file = $root['file.text']';
	
	echo $file->path;      // 'path/to/rootdir/file.txt';
	echo $file->perm;      // '0655';
	echo $file->isFile();  // true;
```

##### … and operate on it
```php

	$file->chmod(0775);	
	$file->chown('newowner');
	$file->group('newowner');	

```	


##### … and operate on it
```php

	$file->chmod(0775);	
	$file->chown('newowner');
	$file->group('newowner');	
	
	// rename a file:	
	$file->rename('newname');
	
	//move a file:
	$file->move('path/newlocation');
	
	// If thers already a file called `file.txt` in `path/newlocation`,
	// `move` will enumearate the new file, e.g. `file copy 1.txt`.
	
	// Override this beahviour in setting `$enum` to false
	$file->move('path/newlocation', false);
	
	// Override default copy name, where the third argument is the enumaration base:
	$file->move('path/newlocation', true, 1, 'Kopie'); // file Kopie 1.txt
	
	// move a file to a different location and rename it:
	$file->move('path/newlocation/newname.txt');
```

##### list directory as array
```php

	$root->toArray();
	
	/*
	It is possible to exclude or ignore directories and files when calling toArray.
	Pass a regular expression as first arguments to ignore directories or files 
	that match a certain pattern, or pass an array containing absolute paths that should 
	be ignored. 
	*/
	
	$root->filter('/(^\..DS_*)/')->toArray();
	$root->filter('/(\.txt)/', ['/path/to/rootdir/newDir'])->toArray();
	$root->filter(null, ['/path/to/rootdir/ignoredDir'])->toArray();
```

#### There is more to come

Meanwhile refere to the doc comments
