<?php

/**
 * This File is part of the Stream\Filesystem package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem;

use Stream\Common\AbstractServiceDriver;

/**
 * @class FilesystemServiceDriver
 */

class FilesystemServiceDriver extends AbstractServiceDriver
{
    /**
     * {@inheritDoc}
     */
    protected function registerService()
    {
        return [
            'filesystem' => $this->app->share(
                function () {
                    return new Filesystem();
                }
            )
        ];
    }
}
