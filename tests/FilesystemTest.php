<?php

/**
 * This File is part of the tests package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Filesystem;

use Stream\Filesystem\Filesystem;
use Stream\Filesystem\FSDirectory;
use org\bovigo\vfs\vfsStream;

/**
 * @class FilesystemTest
 */
class FilesystemTest extends DirectoryProvider
{
    protected $root;

    protected $path;

    protected $object;

    protected function setUp()
    {
        $struct = current(current($this->directoryProvider()));
        $keys = array_keys($struct);

        $this->root = vfsStream::setup('testpath', 0777);
        vfsStream::create($struct, $this->root);
        $this->path = vfsStream::url('testpath/' . $keys[0]);
    }

    /**
     * @var ClassName
     */
    protected function tearDown()
    {

    }

    /**
     * @test
     */
    public function copyDirectoryShouldWorkFlawlessly()
    {
        $fsys = new Filesystem($this->path . '/subdirA');
        $this->assertFalse(is_dir($this->path . '/subdirA copy 1'));
        $fsys->copy();
        $this->assertTrue(is_dir($this->path . '/subdirA copy 1'));
    }

    /**
     * @test
     */
    public function copyRecursiveDirectoryShouldWorkFlawlessly()
    {
        $fsys = new Filesystem($this->path . '/subdirA');
        $fsys->copy();
        $this->assertTrue(is_dir($this->path . '/subdirA copy 1'));
    }

    /**
     * @test
     * @expectedException Stream\Filesystem\Exception\FSIOException
     */
    public function instantioationShouldFailWhenDirectoryDoesNotExist()
    {
        new Filesystem($this->path . '/fuck');
    }

    /**
     * @test
     */
    public function instantioationShouldSucceedWhenDirectoryIsDot()
    {
        $fsys = new Filesystem($this->path . '/.');
        $this->assertEquals($this->path, (string)$fsys->get());
    }

    /**
     * @test
     * @expectedException Stream\Filesystem\Exception\FSIOException
     */
    public function instantioationShouldFailWhenCallingOnAFile()
    {
        new Filesystem($this->path . '/fileA.txt');
    }

    /**
     * @test
     * @skipinclomplete
     */
    public function testRenameDirectory()
    {
        $fsys = new Filesystem($this->path . '/subdirA');
        $this->assertFalse(is_dir($this->path . '/foo'));
        $fsys->rename('foo');
        $this->assertTrue(is_dir($this->path . '/foo'));
    }

    public function testPathModificators()
    {
        $this->assertEquals('/foo/bar', FSDirectory::normalizePath('\foo\bar'));
        $this->assertFalse(FSDirectory::isRelativePath('/foo/bar'));
        $this->assertTrue(FSDirectory::isRelativePath('foo/bar'));
        $this->assertFalse(FSDirectory::isRelativePath('C:\\foo\\bar'));
        $this->assertEquals('/foo/bl', Filesystem::getRealAbstractPath('/foo//bar/../bl'));
        $this->assertEquals('/foo/bar/', Filesystem::getRealAbstractPath('/foo//bar/'));
        $this->assertEquals('File:///foo/bl/', Filesystem::getRealAbstractPath('File:///foo/bar/../bl/'));
        $this->assertEquals('File:///foo/bar/bl/', Filesystem::getRealAbstractPath('File:///foo/bar/./bl/'));
        $this->assertEquals(getcwd() . '/bl', Filesystem::getRealAbstractPath('./bl'));
    }

    /**
     * @test
     */
    public function testMoveLocation()
    {
        $fsys = new Filesystem($this->path . '/subdirA');
        $fsys->move('../subdirB/');
        $this->assertTrue(is_dir($this->path . '/subdirB/subdirA'));

        $fsys->move('../subdirB', '../subdirC/');
        $this->assertTrue(is_dir($this->path . '/subdirC/subdirB'));
    }

    /**
     * @test
     */
    public function testRenameFile()
    {
        $fsys = new Filesystem($this->path . '/subdirA');
        $fsys->get()->recursive()->chmod(0777);
        $fsys->get('fileA1.txt')->rename('foo.txt');
        $this->assertTrue(is_file($this->path . '/subdirA/foo.txt'));
        $fsys->rename('fileB1.txt', 'bar.txt');
        $this->assertTrue(is_file($this->path . '/subdirA/bar.txt'));
    }

    /**
     * @test
     */
    public function testCopyFile()
    {
        $fsys = new Filesystem($this->path);
        $fsys->copy('fileA.txt');
        $this->assertTrue(is_file($this->path . '/fileA copy 1.txt'));
        $fsys->copy('fileA.txt', 'foobar.txt');
        $this->assertTrue(is_file($this->path . '/foobar.txt'));
    }

    /**
     * @test
     */
    public function testRemoveFile()
    {
        $fsys = new Filesystem($this->path);
        $fsys->remove('fileA.txt');
        $this->assertFalse(is_file($this->path . '/fileA.txt'));
    }

    /**
     * @test
     */
    public function testRemoveDirectory()
    {
        $fsys = new Filesystem($this->path . '/subdirA');
        $fsys->remove();
        $this->assertFalse(is_dir($this->path . '/subdirA'));

        $fsys = new Filesystem($this->path);
        $fsys->remove('subdirB');
        $this->assertFalse(is_dir($this->path . '/subdirB'));
    }

    /**
     * testChangeDirectoryPermission
     *
     * @access public
     * @return mixed
     */
    public function testChangeDirectoryPermission()
    {
        $fsys = new Filesystem($this->path);
        $fsys->chmod('subdirA', 0755);

        $this->assertEquals('0755', $fsys->get('subdirA')->perm);
        $this->assertFalse('0755' === $fsys->get('subdirA/subSubdirA1')->perm);
    }

    /**
     * testChangeDirectoryPermissionRecusrive
     *
     * @access public
     * @return mixed
     */
    public function testChangeDirectoryPermissionRecusrive()
    {
        $fsys = new Filesystem($this->path);
        $fsys->chmod('subdirA', 0777, true);

        $this->assertEquals('0777', $fsys->get('subdirA')->perm);

        foreach ($fsys->get('subdirA') as $obj) {
            $this->assertEquals('0777', $obj->perm);
        }
    }
}
