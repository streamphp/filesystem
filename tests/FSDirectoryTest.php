<?php

/**
 * This File is part of the Stream\Filesystem package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Filesystem;

use Stream\Filesystem\AbstractFSObject;
use Stream\Filesystem\FSDirectory;
use Stream\Filesystem\FSFile;
use org\bovigo\vfs\vfsStream;

/**
 * Class: FSDirectoryTest
 *
 * @see PHPUnit_Framework_TestCase
 */
class FSDirectoryTest extends DirectoryProvider
{
    /**
     * @var ClassName
     */
    protected $root;
    protected $path;
    protected $object;

    protected function setUp()
    {
        $struct = current(current($this->directoryProvider()));
        $keys = array_keys($struct);

        $this->root = vfsStream::setup('testpath', 777);
        vfsStream::create($struct, $this->root);
        $this->path = vfsStream::url('testpath/' . $keys[0]);
    }

    /**
     * @test
     * @covers FSDirectory::__construct
     */
    public function testConstruct()
    {
        $dir = new FSDirectory($this->path);
        $this->assertEquals($this->path, (string)$dir);
    }

    /**
     * @test
     * @covers FSDirectory::offsetGet
     * @dataProvider directoryProvider
     */
    public function testArrayAccessGet($structure)
    {
        $dir = new FSDirectory($this->path);

        $this->assertInstanceOf('Stream\Filesystem\FSFile', $dir['fileA.txt']);
        $this->assertInstanceOf('Stream\Filesystem\FSFile', $dir['fileB.txt']);
        $this->assertInstanceOf('Stream\Filesystem\FSDirectory', $dir['subdirA']);
        $this->assertInstanceOf('Stream\Filesystem\FSDirectory', $dir['subdirA']['subSubdirA1']);
        $this->assertInstanceOf('Stream\Filesystem\FSFile', $dir['subdirB']['subSubdirB2']['fileJ.txt']);
    }

    /**
     * chmodRecursiveShouldSuccess
     * @test
     */
    public function chmodRecursiveShouldSuccess()
    {
        $dir = new FSDirectory($this->path . '/subdirA', 0777);
        $dir->recursive()->chmod(0755);

        foreach ($dir as $obj) {
            $this->assertEquals('0755', $obj->perm);
        }
        $dir->chmod(0655);
        $this->assertEquals('0655', $dir->perm);
        $this->assertEquals('0755', $dir['subSubdirA1']->perm);
    }

    /**
     * @test
     * @covers FSDirectory::get
     * @dataProvider directoryProvider
     */
    public function testGet($structure)
    {

        $dir = new FSDirectory($this->path);
        $this->assertTrue($dir->get('subdirA') instanceof FSDirectory);
        $this->assertEquals($this->path . '/subdirA', (string)$dir->get('subdirA'));
        $this->assertTrue($dir->get('subdirA/subSubdirA1') instanceof FSDirectory);
        $this->assertTrue($dir->get('/subdirA/subSubdirA1/') instanceof FSDirectory);
        $this->assertEquals($this->path . '/subdirA/subSubdirA1', (string)$dir->get('subdirA/subSubdirA1'));
        $this->assertTrue($dir->get('subdirA/subSubdirA1/subSubSubdirA1') instanceof FSDirectory);
        $this->assertFalse($dir->get('subdirA/idonotexist') instanceof FSDirectory);
        $this->assertTrue(is_null($dir->get('subdirA/idonotexist')));
        $this->assertTrue($dir->get('fileA.txt') instanceof FSFile);
        $this->assertTrue($dir->get('subdirA/subSubdirA1/fileC.txt') instanceof FSFile);

    }

    /**
     * @test
     * @covers FSDirectory::offsetUnset
     * @dataProvider directoryProvider
     */
    public function testArrayAccessUnset($structure)
    {

        $dir = new FSDirectory($this->path);

        unset($dir['subdirA']);
        unset($dir['fileB.txt']);

        $this->assertFalse(is_dir($this->path . '/subdirA'));
        $this->assertFalse(file_exists($this->path . '/fileB.txt'));
    }

    /**
     * @test
     * @covers FSDirectory::offsetSet
     * @dataProvider directoryProvider
     */
    public function testArrayAccessSet($structure)
    {
        $path = $this->path;
        $dir = new FSDirectory($path);
        $dir[] = 'idaho';
        $this->assertTrue(is_dir($path . '/idaho'));
    }

    /**
     * @test
     * @dataProvider directoryProvider
     * @covers FSDirectory::move
     */
    public function testMoveToItsOwnSubfolderShouldFail($structure)
    {

        $dir = new FSDirectory($this->path);
        $this->assertFalse($dir->move($this->path . '/subdirA'));
    }

    /**
     * @test
     * @covers FSDirectory::move
     * @dataProvider directoryProvider
     */
    public function testMoveToItsOwnSubfolderShouldSucceed($structure)
    {

        $dir = new FSDirectory($this->path . '/subdirA');
        $this->assertTrue(is_dir($this->path . '/subdirA'));

        $this->assertTrue(false !== $dir->move($this->path . '/subdirB', true));
        $this->assertEquals($this->path . '/subdirB copy 1', (string)$dir);

        // test if files are moved
        $this->assertTrue(file_exists($this->path . '/subdirB copy 1/subSubdirA1/fileC.txt'));
        $this->assertFalse(is_dir($this->path . '/subdirA'));

        $this->assertTrue(false !== $dir->move($this->path . '/subdirB/', true));
        $this->assertTrue(file_exists($this->path . '/subdirB/subdirB copy 1/subSubdirA1/fileC.txt'));

    }

    /**
     * @test
     * @covers FSDirectory::__construct
     * @covers FSDirectory::create
     * @expectedException Stream\Filesystem\Exception\FSIOException
     */
    public function testCreateNewDirOnConstruction()
    {
        $path = $this->path . '/idontexistyet';
        new FSDirectory($path, 0777);
    }

    /**
     * @test
     * @covers FSDirectory::remove
     * @dataProvider directoryProvider
     */
    public function testRemoveDirectory($structure)
    {
        $dir = new FSDirectory($this->path . '/subdirA');

        $this->assertTrue(is_dir($this->path . '/subdirA'));
        $this->assertTrue($dir->remove());
        $this->assertFalse(is_dir($this->path . '/subdirA'));
    }

    /**
     * @test
     * @covers FSDirectory::rename
     * @dataProvider directoryProvider
     */
    public function testRename($structure)
    {
        $dir = new FSDirectory($this->path . '/subdirA');
        $dir->rename('newname');

        $this->assertEquals('newname', $dir->name);
        $this->assertEquals('newname', $dir->getChangedAttributes('name'));
        $this->assertEquals($this->path . '/newname', (string)$dir);

    }

    /**
     * @test
     * @covers FSDirectory::touch
     */
    public function testCreateFile()
    {
        $dir = new FSDirectory($this->path, 0777);
        $dir->touch('newfile', 0777);
        $this->assertTrue(file_exists($this->path . '/newfile'));
    }

    /**
     * @test
     * @covers FSDirectory::toArray
     * @dataProvider directoryProvider
     */
    public function testIgnoreFiles($structure)
    {

        $regexp = '/(^\..*|\.jpe?g$)/';
        $dir = new FSDirectory($this->path, 0777);
        $list = $dir->filter($regexp)->toArray();

        foreach ($list['files'] as $file) {
            if (preg_match($regexp, $file['name'])) {
                $this->fail('expected no file like ' . $file['name']);
                break;
            }
        }
    }

    /**
     * @test
     * @dataProvider directoryProvider
     */
    public function testDirectoryIterate($structure)
    {
        $dir = new FSDirectory($this->path, 0777);
        $tree = $structure[basename($this->path)];

        foreach ($dir as $key => $d) {
            $this->assertTrue(isset($tree[$key]));
            $this->assertTrue($d instanceof AbstractFSObject);
        }
    }

    /**
     * @test
     * @covers FSDirectory::listStructure
     * @dataProvider directoryProvider
     */
    public function testExcludeDirs($structure)
    {

        $excludes = array($this->path . '/subdirA/subSubdirA1', $this->path . '/subdirB/subSubdirB2');
        $dir = new FSDirectory($this->path, 0777);
        $list = $dir->filter(null, $excludes)->toArray();

        foreach ($list['directories'] as $dir) {

            if ($dir['attributes']['name'] === 'subdirA' || $dir['attributes']['name'] === 'subdirB') {

                foreach ($dir['directories'] as $subdir) {
                    if (in_array($subdir['attributes']['path'], $excludes)) {
                        $this->fail('Expected ' . $subdir['attributes']['path'] . ' to be ignored');
                        break;
                    }
                }
            }
        }
    }

    /**
     * @test
     * @covers FSDirectory::permission
     * @dataProvider directoryProvider
     */
    public function testChangePermission($structure)
    {
        $dir = new FSDirectory($this->path, 0777);
        $dir['subdirA']->permission(0775);

        $this->assertEquals('0775', $dir['subdirA']->permission());

    }

    /**
     * @test
     * @covers FSDirectory::mkdir
     * @dataProvider directoryProvider
     */
    public function testMkdir($structure)
    {
        $dir = new FSDirectory($this->path, 0777);

        $dir->mkdir();
        $this->assertTrue(isset($dir['unnamed folder']));

        $dir->mkdir();
        $this->assertTrue(isset($dir['unnamed folder 2']));

    }

    /**
     * @test
     * @covers FSDirectory::mkdir
     * @expectedException Stream\Filesystem\Exception\FSIOException
     */
    public function testMkdirShouldNotCreateNewDirectories()
    {
        $dir = new FSDirectory($this->path . '/subdirA', 0777);
        $dir->mkdir('newdir/newdirA/newdirB', 0775, false);
    }

    /**
     * @test
     * @covers FSDirectory::mkdir
     * @dataProvider directoryProvider
     */
    public function testMkdirRecursive($structure)
    {
        $dir = new FSDirectory($this->path, 0777);

        $dir->mkdir('newdir/newdirA/newdirB', 0775, true);

        $this->assertTrue(isset($dir['newdir']));
        $this->assertTrue(isset($dir['newdir']['newdirA']));
        $this->assertTrue(isset($dir['newdir']['newdirA']['newdirB']));
    }

    /**
     * testDirectoryShouldBeEmpty
     * @test
     * @covers FSDirectory::isEmpty
     */
    public function testDirectoryShouldBeEmpty()
    {
        $dir = new FSDirectory($this->path . '/empty dir');
        $this->assertTrue($dir->isEmpty());
    }

    /**
     * @test
     * @covers FSDirectory::ensureExists
     * @dataProvider directoryProvider
     */
    public function testEnsureDirectoryExsitsWithNoneExistingDir($structure)
    {

        $dir = new FSDirectory($this->path, 0777);

        $isDirectoryCreated = $dir->ensureExists('idonotexist');

        $this->assertTrue($isDirectoryCreated);
        $this->assertTrue(isset($dir['idonotexist']));

        $isDirectoryCreated = $dir->ensureExists('someotherdir/foo');

        $this->assertTrue($isDirectoryCreated);
        $this->assertTrue(isset($dir['someotherdir']['foo']));
    }

    /**
     * testPurgeFilesFlat
     * @test
     */
    public function testPurgeFilesFlat()
    {
        $dir = new FSDirectory($this->path . DIRECTORY_SEPARATOR . 'subdirA', 0777);
        $dir->purgeFiles();

        $this->assertFalse(is_file($this->path . DIRECTORY_SEPARATOR . 'subdirA' . DIRECTORY_SEPARATOR . 'fileA.txt'));
        $this->assertFalse(is_file($this->path . DIRECTORY_SEPARATOR . 'subdirA' . DIRECTORY_SEPARATOR . 'fileB.txt'));
        $this->assertTrue(is_file($this->path . DIRECTORY_SEPARATOR . 'subdirA' . DIRECTORY_SEPARATOR . 'subSubdirA1' . DIRECTORY_SEPARATOR . 'fileC.txt'));
    }

    /**
     * testPurgeFilesRecursive
     * @test
     */
    public function testPurgeFilesRecursive()
    {
        $dir = new FSDirectory($this->path . DIRECTORY_SEPARATOR . 'subdirA', 0777);
        $dir->purgeFiles(true);

        $this->assertFalse(is_file($this->path . DIRECTORY_SEPARATOR . 'subdirA' . DIRECTORY_SEPARATOR . 'fileA.txt'));
        $this->assertFalse(is_file($this->path . DIRECTORY_SEPARATOR . 'subdirA' . DIRECTORY_SEPARATOR . 'subSubdirA1' . DIRECTORY_SEPARATOR . 'fileC.txt'));
    }

    /**
     * @test
     * @covers FSDirectory::unsureExists
     * @dataProvider directoryProvider
     */
    public function testEnsureDirectoryExsitsWithExistingDir($structure)
    {

        $dir = new FSDirectory($this->path, 0777);

        $isDirectoryCreated = $dir->ensureExists('subdirA');
        $this->assertTrue($isDirectoryCreated);
        $isDirectoryCreated = $dir->ensureExists('subdirA/subSubdirA1');
        $this->assertTrue($isDirectoryCreated);
    }

    /**
     * @test
     * @covers FSDirectory::unsureExists
     * @dataProvider directoryProvider
     */
    public function testEnsureDirectoryExsitsWithNoneExistingFile($structure)
    {

        $dir = new FSDirectory($this->path, 0777);
        $isFileCreated = $dir->ensureExists('fileFoo.txt', true);
        $this->assertTrue($isFileCreated);
        $this->assertTrue($dir->get('fileFoo.txt')->isFile());

        $isFileCreated = $dir->ensureExists('foo/fileFoo.txt', true);

        $this->assertTrue($isFileCreated);
        $this->assertTrue($dir->get('foo/fileFoo.txt')->isFile());
    }
}
