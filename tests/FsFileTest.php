<?php

/**
 * This File is part of the Stream\Filesystem package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Filesystem;

use Stream\Filesystem\AbstractFSObject;
use Stream\Filesystem\FSFile;
use org\bovigo\vfs\vfsStream;

class FSFileTest extends \PHPUnit_Framework_TestCase
{
    /**
     * root
     *
     * @var Mixed
     * @access protected
     */
    protected $root;

    /**
     * testpath
     *
     * @var Mixed
     * @access protected
     */
    protected $testpath;

    /**
     * setUp
     *
     * @access protected
     * @return void
     */
    protected function setUp()
    {
        $this->root = vfsStream::setup('testpath', 755);
        $this->testpath = vfsStream::url('testpath');
        //dirname(__FILE__) . DIRECTORY_SEPARATOR . 'testpath';

        if (!is_dir($this->testpath)) {

            mkdir($this->testpath, 0777);
            chmod($this->testpath, 0777);

        }
    }

    /**
     * tearDown
     *
     * @access protected
     * @return void
     */
    protected function tearDown()
    {
        $this->cleanUpDirs($this->testpath);
    }


    protected function cleanUpDirs($path)
    {

        foreach (glob($path . DIRECTORY_SEPARATOR . '*') as $item) {
            if (is_file($item)) {
                unlink($item);
            }

            if (is_dir($item)) {
                $this->cleanUpDirs($item);
            }
        }
    }

    /**
     * makeFile
     *
     * @param string $file
     * @access protected
     * @return void
     */
    protected function makeFile($file = 'testfile.txt')
    {

        $file = $this->testpath . DIRECTORY_SEPARATOR . $file;

        if (!file_exists($file)) {
            vfsStream::newFile($file, 0777);
            file_put_contents($file, 'some string');
        }
        FSFile::setMimeSniffer($this->getMockedMimeSniffer($file));

        return new FSFile($file);
    }

    /**
     * getMockedMimeSniffer
     *
     * @access protected
     * @return void
     */
    protected function getMockedMimeSniffer($file, $mime = true)
    {
        $sniffer = $this->getMock('Stream\Filesystem\MIME\InterfaceMimeSniffer', array('getMime'));

        $sniffer->expects($this->any())
                ->method('getMime')
                ->will($this->returnValue($mime ? finfo_file(finfo_open(FILEINFO_MIME_TYPE), $file) : 'text/plain'));
        return $sniffer;
    }

    /**
     * testRename
     *
     * @covers FSFile::rename
     * @test
     */
    public function testRename()
    {
        $file = $this->makeFile();
        $oldfile = (string)$file;

        $file->rename('foo.txt');

        $this->assertFalse(file_exists($this->testpath . DIRECTORY_SEPARATOR . $oldfile));
        $this->assertTrue(file_exists($this->testpath . DIRECTORY_SEPARATOR . 'foo.txt'));
        $this->assertEquals($this->testpath . DIRECTORY_SEPARATOR . 'foo.txt', (string)$file);
    }

    /**
     * testRenameShouldUpdateFileExtension
     *
     * @covers FSFile::rename
     * @test
     */
    public function testRenameShouldUpdateFileExtension()
    {
        $file = $this->makeFile();
        $oldfile = (string)$file;

        $file->rename('foo.jpg');

        $this->assertFalse(file_exists($this->testpath . DIRECTORY_SEPARATOR . $oldfile));
        $this->assertTrue(file_exists($this->testpath . DIRECTORY_SEPARATOR . 'foo.jpg'));
        $this->assertEquals($this->testpath . DIRECTORY_SEPARATOR . 'foo.jpg', (string)$file);
        $this->assertEquals('jpg', $file->extension);
    }

    /**
     * @test
     */
    public function testMime()
    {
        $file = $this->makeFile();
        $this->assertEquals('text/plain', $file->mimetype);
    }

    /**
     * @test
     * @covers FSFile::put
     */
    public function testPut()
    {
        $file = $this->makeFile();
        $file->put('new content', null);
        $this->assertEquals('new content', file_get_contents($file));
    }

    /**
     * @test
     * @covers FSFile::contents
     */
    public function testContents()
    {
        $file = $this->makeFile();
        $file->put('new content', null);
        $this->assertEquals('new content', $file->contents());
    }

    /**
     * @test
     * @covers FSFile::permission
     */
    public function testPermission()
    {
        $file = $this->makeFile();

        $file->permission(0644);
        $this->assertEquals('0644', $file->perm);
        $this->assertEquals('0644', sprintf('0%o', $this->root->getChild($file->name)->getPermissions()));

        $file->permission(0775);
        $this->assertEquals('0775', $file->perm);
        $this->assertEquals('0775', sprintf('0%o', $this->root->getChild($file->name)->getPermissions()));

        $file->permission(0777);
        $this->assertEquals('0777', $file->perm);
        $this->assertEquals('0777', $file->permission());
        $this->assertEquals('0777', sprintf('0%o', $this->root->getChild($file->name)->getPermissions()));
    }

    /**
     * @test
     * @covers FSFile::remove
     */
    public function testRemove()
    {
        $file = $this->makeFile();
        $path = (string)$file;
        $file->remove();
        $this->assertFalse(file_exists($path));
    }

    /**
     * @test
     * @covers FSFile::exists
     * @covers FSFile::remove
     */
    public function testExsits()
    {
        $file = $this->makeFile();
        $this->assertTrue($file->exists());
        $file->remove();
        $this->assertFalse($file->exists());
    }

    /**
     * testNove
     *
     * @covers FSFile::remove
     */
    public function testMove()
    {
        $file = $this->makeFile();
        vfsStream::create(array('new' => array()));
        $newdir = $this->testpath . '/new';

        $this->assertTrue(false !== $file->move($newdir . '/renamed.txt'));
        $this->assertTrue($this->root->getChild('new')->hasChild('renamed.txt'));
        $this->assertTrue($file->exists());
    }

    /**
     * testMoveWhenNewFileAlreadyExists
     *
     * @covers FSFile::remove
     * @covers FSFile::enum
     *
     * @test
     */
    public function testMoveWhenNewFileAlreadyExists()
    {
        $file = $this->makeFile();
        vfsStream::create(array('new' => array()));
        $newdir = $this->testpath . '/new';

        touch($newdir . '/renamed.txt');
        touch($newdir . '/renamed copy.txt');
        touch($newdir . '/renamed copy 1.txt');
        touch($newdir . '/renamed copy 2.txt');
        touch($newdir . '/renamed copy 4.txt');

        $this->assertFalse($file->move($newdir . '/renamed.txt', false));
        $this->assertTrue(false !== $file->move($newdir . '/renamed.txt'));
        $this->assertEquals('renamed copy 3.txt', $file->name);
    }

    /**
     * testChangedAttributesPermission
     * @covers FSFile::getChangedAttributes
     *
     * @access public
     * @test
     */
    public function testChangedAttributesPermission()
    {
        $file = $this->makeFile();
        $file->permission(0644);
        $this->assertTrue(is_array($file->getChangedAttributes()));
        $this->assertEquals(1, count($file->getChangedAttributes()));
        $this->assertContains('0644', $file->getChangedAttributes());
        $this->assertArrayHasKey('perm', $file->getChangedAttributes());
    }

    /**
     * testGetOriginalAttributes
     *
     * @covers FSFile::getOriginalAttributes
     * @test
     */
    public function testGetOriginalAttributes()
    {

        $file = $this->makeFile();
        $attribute = $file->getOriginalAttributes();
        $file->permission(0644);
        $this->assertSame($attribute, $file->getOriginalAttributes());
        $this->assertFalse($file->perm === $file->getOriginalAttributes('perm'));
    }

    //public function testCreateFile()
    //{
    //    $fname = $this->testpath . '/fileA.txt';
    //    $file = new FSFile($fname, 0775);
    //    $this->assertTrue(file_exists((string)$file));
    //}
    /**
     * testCreateFile
     *
     * @test
     * @expectedException Stream\Filesystem\Exception\FSIOException
     */
    public function testCreateFileShouldRaiseException()
    {
        $fname = $this->testpath . '/newfile.txt';
        $file = new FSFile($fname, 0775);
        $this->assertTrue(file_exists((string)$file));
    }

    /**
     * testCreateFile
     *
     * @covers FSFile::create
     * @test
     * @expectedException Stream\Filesystem\Exception\FSIOException
     */
    public function testCreateFileThrowsException()
    {
        $fname = $this->testpath . '/deadend/doomed.txt';
        new FSFile($fname, 0775);
    }
}
