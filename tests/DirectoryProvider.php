<?php

/**
 * This File is part of the Stream\Filesystem package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */
namespace Stream\Tests\Filesystem;

use Stream\Filesystem\AbstractFSObject;
use Stream\Filesystem\FSDirectory;
use Stream\Filesystem\FSFile;
use org\bovigo\vfs\vfsStream;

abstract class DirectoryProvider extends \PHPUnit_Framework_TestCase
{
    /**
     * directoryProvider
     *
     * @provider
     */
    public function directoryProvider()
    {
        return $this->getDataSet();
    }

    public function getDataSet()
    {
        $xml = simplexml_load_file(dirname(__FILE__) . '/Fixures/FSDirTree.xml');

        $tree = $this->parseTree($xml);
        return [[$tree]];
    }

    protected function parseTree($xml, &$tree = [])
    {

        foreach ($xml as $i => $el) {
            $type = (string)$el->attributes()->type;
            $name = (string)$el->attributes()->name;

            if ($type === 'dir') {
                $tree[$name] = isset($tree[$name]) ? $tree[$name] : [];
                $this->parseTree($el, $tree[$name]);
            }

            if ($type === 'file') {
                $tree[$name] = (string)$el;
            }
        }

        return $tree;
    }

    protected function createStructure(array $struct, $dir)
    {
        foreach ($struct as $name => $item) {
            if (is_array($item)) {
                mkdir($dir . DIRECTORY_SEPARATOR . $name);
                $this->createStructure($item, $dir . DIRECTORY_SEPARATOR . $name);
            } elseif (is_string($item)) {
                file_put_contents($dir . DIRECTORY_SEPARATOR . $name, $item);
            }
        }
    }

    protected function tearDownStructure($dir)
    {
        foreach (glob($dir . '{/.*,/*}', GLOB_BRACE | GLOB_NOSORT) as $path => $item) {

            $name = basename($item);

            if ($name === '.' || $name === '..') {
                continue;
            }

            if (is_file($item)) {
                unlink($item);
            }

            if (is_dir($item)) {
                $this->tearDownStructure($item);
                rmdir($item);
            }

        };
    }
}
